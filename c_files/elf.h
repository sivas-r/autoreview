/*--------------------------------------------------------------------------
**
** Copyright 2016 Rockwell Collins. All rights reserved.
** Rockwell Collins Proprietary Information.
**
** File:  elf.h
**
** Description:
**     Definitions for Executable and Loadable Format (ELF) functions.
**
**--------------------------------------------------------------------------
*/
#ifndef ELF_H
#define ELF_H

/*--------------------------------------------------------------------------
**                     Include Files
**--------------------------------------------------------------------------
*/

#include "efi.h"
#include "efi_load_options.h"

/*--------------------------------------------------------------------------
**                     Literal Declarations & Macros
**--------------------------------------------------------------------------
*/

#define EI_NIDENT  16

#define ELF_IDENT  "\177ELF"   /* ASCII DEL \x7F + "ELF" */

/*--------------------------------------------------------------------------
**                     Type definitions
**--------------------------------------------------------------------------
*/

typedef void (*entry_point_fp)(void);

typedef UINT32   ELF32_ADDR;
typedef UINT16   ELF32_HALF;
typedef UINT32   ELF32_OFF;
typedef INT32    ELF32_SWORD;
typedef UINT32   ELF32_WORD;
typedef UINT8    ELF32_BYTE;

typedef struct {
  struct elf32_ehdr *ELFheader;
  struct elf32_phdr *PgmHeader;
  struct elf32_dyn  *DynTbl;
  UINT32 pgmLoadIdx;
} ELF_MODULE_TYPE;

typedef enum
{
  ELF_NO_ERROR = 0,
  ELF_INCOMPATIBLE_FILE_TYPE,
  ELF_INCORRECT_FILE_IDENT,
  ELF_INCORRECT_MODULE_ARCH,
  ELF_UNKNOWN_FILE_TYPE,
  JUMP_TO_ELF_FAILED,
  ELF_MEMORY_ALLOCATION_FAIL,
  ELF_KNOWN_SEC_SIZE_ERROR,
  ELF_RELOCATION_FAIL,
  ELF_SHARED_LIB_LOAD_FAIL,
  ELF_SHARED_SYM_GET_FAIL
} ELF_LOAD_ERROR_TYPE;


/*******************************************************
** ELF Header
********************************************************
**
** ELF Header and other structures conform
** to Tool Interface Standard format.
*/
typedef struct elf32_ehdr
{
  ELF32_BYTE   e_ident[EI_NIDENT];  /* ELF identification */
  ELF32_HALF   e_type;              /* object file type */
  ELF32_HALF   e_machine;           /* architecture */
  ELF32_WORD   e_version;           /* object file version */
  ELF32_ADDR   e_entry;             /* virtual start address */
  ELF32_OFF    e_phoff;             /* prog hdr table offset (in bytes) */
  ELF32_OFF    e_shoff;             /* sect hdr table offset */
  ELF32_WORD   e_flags;             /* processor flags */
  ELF32_HALF   e_ehsize;            /* ELF header size (in bytes) */
  ELF32_HALF   e_phentsize;         /* program header size */
  ELF32_HALF   e_phnum;             /* number of entries in phdr table */
  ELF32_HALF   e_shentsize;         /* section header size */
  ELF32_HALF   e_shnum;             /* number of entries in shdr table */
  ELF32_HALF   e_shstrndx;          /* string table index in shdr table */
} ELF32_EHDR_TYPE;

#define ELFCLASS32      1    /* e_ident EI_CLASS file Class for 32-bit architectures */
#define ELFDATA2LSB     1    /* e_ident EI_DATA little endian Data encoding */

/* e_type  object file type definitions */
#define ET_NONE         0    /* No file type */
#define ET_REL          1    /* Relocatable file */
#define ET_EXEC         2    /* Executable file */
#define ET_DYN          3    /* Shared object file */
#define ET_CORE         4    /* Core file */
#define ET_LOPROC  0xff00    /* Processor-specific */
#define ET_HIPROC  0xffff    /* Processor-specific */

/* e_machine  machine architecture definitions */
#define EM_NONE         0    /* No machine */
#define EM_ARM       0x28    /* ARM processor */

/*******************************************************
** Program Header
********************************************************
*/
typedef struct elf32_phdr
{
  ELF32_WORD     p_type;            /* segment categorization */
  ELF32_OFF      p_offset;          /* segment offset from file beginning */
  ELF32_ADDR     p_vaddr;           /* virtual address of first byte */
  ELF32_ADDR     p_paddr;           /* unspecified contents */
  ELF32_WORD     p_filesz;          /* segment file image size (in bytes) */
  ELF32_WORD     p_memsz;           /* bytes in segment memory image */
  ELF32_WORD     p_flags;           /* flags relevant to segment */
  ELF32_WORD     p_align;           /* segment alignment constraints */
} ELF32_PHDR_TYPE;

/* p_type  segment types */
#define PT_NULL               0   /* signifies unused array element */
#define PT_LOAD               1   /* specifies a loadable segment */
#define PT_DYNAMIC            2   /* dynamic linking information */
#define PT_INTERP             3   /* path data for an interpreter */
#define PT_NOTE               4   /* location and size of auxiliary info */
#define PT_SHLIB              5   /* unspecified reserved segment */
#define PT_PHDR               6   /* location and size of pgm hdr table */
#define PT_LOPROC    0x70000000   /* processor-specific */
#define PT_HIPROC    0x7fffffff   /* processor-specific */

/* p_flags segment attributes */
#define PF_X                 0x1
#define PF_W                 0x2
#define PF_R                 0x4

/*******************************************************
** Section Header
********************************************************
*/
typedef struct elf32_shdr
{
  ELF32_WORD     sh_name;           /* section name */
  ELF32_WORD     sh_type;           /* section categorization */
  ELF32_WORD     sh_flags;          /* miscellaneous attributes */
  ELF32_ADDR     sh_addr;           /* address of first byte */
  ELF32_OFF      sh_offset;         /* first byte offset */
  ELF32_WORD     sh_size;           /* section size (in bytes) */
  ELF32_WORD     sh_link;           /* shdr table index link */
  ELF32_WORD     sh_info;           /* extra information */
  ELF32_WORD     sh_addralign;      /* address alignment constraints */
  ELF32_WORD     sh_entsize;        /* table entry size */
} ELF32_SHDR_TYPE;

/* sh_type  section types */
#define SHT_NULL              0   /* section header is inactive */
#define SHT_PROGBITS          1   /* section info defined by program */
#define SHT_SYMTAB            2   /* symbol table */
#define SHT_STRTAB            3   /* string table */
#define SHT_RELA              4   /* relocation entries with explicit addends*/
#define SHT_HASH              5   /* symbol hash table */
#define SHT_DYNAMIC           6   /* dynamic linking info */
#define SHT_NOTE              7   /* notes */
#define SHT_NOBITS            8   /* sh_offset contains conceptual file offset */
#define SHT_REL               9   /* relocation entries wo explicit addends */
#define SHT_SHLIB            10   /* section does not conform to ABI */
#define SHT_DYNSYM           11   /* dynamic symbol table */
#define SHT_LOPROC   0x70000000   /* processor-specific */
#define SHT_HIPROC   0x7fffffff   /* processor-specific */
#define SHT_LOUSER   0x80000000   /* index range reserved for applications */
#define SHT_HIUSER   0xffffffff   /* index range reserved for applications */

/*sh_flag Attributes */

#define SHF_WRITE    0x1
#define SHF_ALLOC    0x2
#define SHF_EXECINSTR  0x4
#define SHF_MASKPROC 0xf0000000

/*******************************************************
** Symbol Table
********************************************************
*/
typedef struct elf32_sym
{
  ELF32_WORD     st_name;           /* symbol string table index */
  ELF32_ADDR     st_value;          /* context dependent value */
  ELF32_WORD     st_size;           /* symbol size (in bytes) */
  ELF32_BYTE     st_info;           /* type and binding attribute */
  ELF32_BYTE     st_other;          /* currently undefined (0) */
  ELF32_HALF     st_shndx;          /* relevant section hdr table index */
} Elf32_Sym;


/*******************************************************
** Relocations
********************************************************
*/
/* The following are used with relocations */
#define ELF32_R_SYM(x) ((x) >> 8)
#define ELF32_R_TYPE(x) ((x) & 0xff)

/*Dynamic Relocation Type */
#define R_ARM_RELATIVE    23
#define R_ARM_GLOB_DAT    21
#define R_ARM_JUMP_SLOT   22
#define R_ARM_ABS32       02

typedef struct elf32_rel {
  ELF32_ADDR  r_offset;
  ELF32_WORD  r_info;
} Elf32_Rel;

/*******************************************************
** Dynamic sections
********************************************************
*/
/* The following are used with relocations */
typedef struct elf32_dyn {
  ELF32_SWORD d_tag;
  union {
    ELF32_WORD d_val;
    ELF32_ADDR d_ptr;
  } d_un;
} Elf32_Dyn;

/* Dynamic tags */
#define DT_NULL         00                /* dynamic entry is inactive */
#define DT_NEEDED       01                /* needed shared libraries to run the executable  */
#define DT_PLTRELSZ     02                /* plt relocation size */
#define DT_PLTGOT       03                /* plt got table address */
#define DT_HASH         04                /* hash table address */
#define DT_STRTAB       05                /* string table address */
#define DT_SYMTAB       06                /* symbol table address */
#define DT_RELA         07                /* DT_RELA address */
#define DT_RELASZ       08                /* DT_RELA total size */
#define DT_RELAENT      09                /* DT_RELA relocation entry size */
#define DT_STRSZ        10                /* string table size */
#define DT_SYMENT       11                /* symbol table entry size */
#define DT_INIT         12                /* address of init function */
#define DT_FINI         13                /* address of termination function */
#define DT_SONAME       14                /* string table offset of null-terminated string */
#define DT_RPATH        15                /* string table offset of a null-terminated search library */
#define DT_SYMBOLIC     16                /* in shared library, change symbol resolution algorithm */
#define DT_REL          17                /* DT_REL table address */
#define DT_RELSZ        18                /* DT_REL table size */
#define DT_RELENT       19                /* DT_REL entry size */
#define DT_PLTREL       20                /* PLT table address */
#define DT_DEBUG        21                /* debug info */
#define DT_TEXTREL      22                /* text area relocation table */
#define DT_JMPREL       23                /* plt relocation table */
#define DT_LOPROC       0x70000000        /* processor-specific */
#define DT_HIPROC       0x7fffffff        /* processor-specific */

/*--------------------------------------------------------------------------
**                     Function Prototypes
**--------------------------------------------------------------------------
*/
/*-------------------------------------------------------------------------------------------
** Name:    load_elf_module
**
** Syntax:  ELF_LOAD_ERROR_TYPE load_elf_module(ELF32_ADDR *elf_addr)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  Loads an executable ELF module from a predefined memory location.
**
** Algorithm:   Validate ELF file and determine type
**              Load appropriate program header contents
**              Copy [.text] and [.data] sections from ELF exe file
**              Initialize [.bss] section with zeroes
**              Jump to entry point of newly loaded ELF executable
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE load_elf_module(ELF32_ADDR *elf_addr, EFI_LOAD_OPTIONS *load_options);

/*-------------------------------------------------------------------------------------------
** Name:    load_segment
**
** Syntax:  ELF_LOAD_ERROR_TYPE load_segment(ELF_MODULE_TYPE *ElfMod, EFI_IMAGE_ENTRY_POINT *elf_entry)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  Loads an shared PIE/PIC ELF module from a predefined memory location.
**
** Algorithm:   Copy [.text] and [.data] sections from ELF exe file
**              Initialize [.bss] section with zeroes
**              Jump to entry point of newly loaded ELF executable
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE load_segment(ELF_MODULE_TYPE *ElfMod, EFI_LOAD_OPTIONS *load_options);


#endif /* ELF_H */
