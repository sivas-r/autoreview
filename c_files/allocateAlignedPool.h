#ifndef __ALLOCATEALIGNEDPOOL_H__
#define __ALLOCATEALIGNEDPOOL_H__

/*----------------------------- FILE PROLOGUE ------------------------------*/

/*
 *****************************************************************************
 *
 * FILE NAME:
 *   allocateAlignedPool.h
 *
 * PURPOSE:
 *   The AlignedPool APIs align the memory allocated for the pool if it is
 *   not aligned.
 *
 *****************************************************************************
 */

/*----------------------------- FILE INCLUSION -----------------------------*/
#include "efi.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- MACRO DEFINITIONS ----------------------------*/



/*--------------------------- TYPE DECLARATIONS ----------------------------*/



/*------------------------- FUNCTION DECLARATIONS --------------------------*/



/*
 *****************************************************************************
 * NAME:
 *   AllocateAlignedPool
 *
 * PURPOSE:
 *   This API provides an interface to allocate the memory for the pool using
 *   EFI_BOOT_SERVICES.AllocatePool() and aligns the successfully allocated
 *   memory as per the requested alignment.
 *
 * PARAMETERS:
 *   INPUT:
 *     pSysTbl   : Address of executing image system table.
 *     poolType  : Type of memory to allocate
 *     alignment : ppBuffer alignment in bytes
 *     size      : Size of the buffer to allocate in bytes.
 *     [Commentary:
 *       Valid pSysTbl is the EFI_SYSTEM_TABLE Containing pointers to the
 *         runtime and boot services tables.
 *       Refer [SPEC-UEFI] for the possible memory poolType.]
 *
 *   OUTPUT:
 *     ppBuffer : Address of the allocated buffer and is aligned based on the
 *       input alignment.
 *
 * RETURNS:
 *     EFI_SUCCESS           : The buffer is allocated and aligned based on
 *       the inputs size and alignment.
 *     EFI_INVALID_PARAMETER : If any of the following are TRUE.
 *       Parameter pSysTbl is NULL
 *       Parameter size is 0
 *       Parameter alignment is 0
 *       (size + alignment - 1) wraps around
 *       when AllocatePool() returns EFI_INVALID_PARAMETER i.e., if PoolType
 *         (poolType) is invalid or Buffer (ppBuffer) is NULL.
 *     EFI_OUT_OF_RESOURCES  : AllocatePool() could not allocate memory for
 *       the required poolType.
 *
 * NOTES:
 *   SEQUENCING:
 *     None
 *
 *   LIMITATIONS:
 *     The pools returned by this function cannot be freed.
 *
 *   THREAD SAFE:
 *     This function's thread safety depends on the underlying AllocatePool()
 *     implementation.
 *
 *****************************************************************************
 */

EFI_STATUS AllocateAlignedPool (
          IN    EFI_SYSTEM_TABLE *pSysTbl,
          IN    EFI_MEMORY_TYPE poolType,
          IN    UINTN alignment,
          IN    UINTN size,
          OUT   VOID **ppBuffer);

#ifdef __cplusplus
}
#endif

#endif /* __ALLOCATEALIGNEDPOOL_H__ */
