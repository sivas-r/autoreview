/*--------------------------------------------------------------------------
**
** Copyright 2016 Rockwell Collins. All rights reserved.
** Rockwell Collins Proprietary Information.
**
** File:  efi.h
**
** Description:
**
**--------------------------------------------------------------------------
*/
#ifndef EFI_H
#define EFI_H

/*--------------------------------------------------------------------------
**                     Include Files
**--------------------------------------------------------------------------
*/

#include "platform_typedef.h"

/*--------------------------------------------------------------------------
**                     Literal Declarations & Macros
**--------------------------------------------------------------------------
*/

#ifndef NULL
  #define NULL '\0'
#endif

#ifndef TRUE
#define TRUE 1
#endif // !TRUE

#ifndef FALSE
#define FALSE 0
#endif // !FALSE



#define EFI_SYSTEM_TABLE_SIGNATURE 0x5453595320494249
#define EFI_2_50_SYSTEM_TABLE_REVISION ((2<<16) | (50))
#define EFI_2_40_SYSTEM_TABLE_REVISION ((2<<16) | (40))
#define EFI_2_31_SYSTEM_TABLE_REVISION ((2<<16) | (31))
#define EFI_2_30_SYSTEM_TABLE_REVISION ((2<<16) | (30))
#define EFI_2_20_SYSTEM_TABLE_REVISION ((2<<16) | (20))
#define EFI_2_10_SYSTEM_TABLE_REVISION ((2<<16) | (10))
#define EFI_2_00_SYSTEM_TABLE_REVISION ((2<<16) | (00))
#define EFI_1_10_SYSTEM_TABLE_REVISION ((1<<16) | (10))
#define EFI_1_02_SYSTEM_TABLE_REVISION ((1<<16) | (02))
#define EFI_SPECIFICATION_VERSION EFI_SYSTEM_TABLE_REVISION
#define EFI_SYSTEM_TABLE_REVISION EFI_2_50_SYSTEM_TABLE_REVISION


#define EFI_BOOT_SERVICES_SIGNATURE 0x56524553544f4f42
#define EFI_BOOT_SERVICES_REVISION EFI_SPECIFICATION_VERSION

#define EFI_RUNTIME_SERVICES_SIGNATURE 0x56524553544e5552
#define EFI_RUNTIME_SERVICES_REVISION EFI_SPECIFICATION_VERSION

/* GUIDs */
#define EFI_ACPI_20_TABLE_GUID \
  {0x8868e871,0xe4f1,0x11d3,\
  {0xbc,0x22,0x00,0x80,0xc7,0x3c,0x88,0x81}}
#define ACPI_TABLE_GUID \
  {0xeb9d2d30,0x2d88,0x11d3,\
  {0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d}}
#define SAL_SYSTEM_TABLE_GUID \
  {0xeb9d2d32,0x2d88,0x11d3,\
  {0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d}}
#define SMBIOS_TABLE_GUID \
  {0xeb9d2d31,0x2d88,0x11d3,\
  {0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d}}
#define SMBIOS3_TABLE_GUID \
  {0xf2fd1544, 0x9794, 0x4a2c,\
  {0x99,0x2e,0xe5,0xbb,0xcf,0x20,0xe3,0x94})
#define MPS_TABLE_GUID \
  {0xeb9d2d2f,0x2d88,0x11d3,\
  {0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d}}

/* ACPI 2.0 or newer tables should use EFI_ACPI_TABLE_GUID */
#define EFI_ACPI_TABLE_GUID \
  {0x8868e871,0xe4f1,0x11d3,\
  {0xbc,0x22,0x00,0x80,0xc7,0x3c,0x88,0x81}}
#define ACPI_10_TABLE_GUID \
  {0xeb9d2d30,0x2d88,0x11d3,\
  {0x9a,0x16,0x00,0x90,0x27,0x3f,0xc1,0x4d}}
#define EFI_PROPERTIES_TABLE_GUID \
  {0x880aaca3, 0x4adc, 0x4a04,\
  {0x90,0x79,0xb7,0x47,0x34,0x8,0x25,0xe5}}

/* Console Support */
#define EFI_SIMPLE_TEXT_INPUT_PROTOCOL_GUID \
  {0x387477c1,0x69c7,0x11d2,\
  {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}
#define EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL_GUID \
  {0x387477c2,0x69c7,0x11d2,\
  {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}

#define EFI_DEVICE_PATH_PROTOCOL_GUID \
  {0x09576e91,0x6d3f,0x11d2,\
  {0x8e,0x39,0x00,0xa0,0xc9,0x69,0x72,0x3b}}


#define EFI_PROPERTIES_TABLE_VERSION 0x00010000

/* Memory attribute (Not defined bits are reserved) */
#define EFI_PROPERTIES_RUNTIME_MEMORY_PROTECTION_NON_EXECUTABLE_PE_DATA 0x1 /* BIT 0 - description - implies the runtime data is separated from the code */


/* Event Types */
#define EVT_TIMER                         0x80000000u
#define EVT_RUNTIME                       0x40000000u
#define EVT_NOTIFY_WAIT                   0x00000100u
#define EVT_NOTIFY_SIGNAL                 0x00000200u
#define EVT_SIGNAL_EXIT_BOOT_SERVICES     0x00000201u
#define EVT_SIGNAL_VIRTUAL_ADDRESS_CHANGE 0x60000202u

/* Prototype Argument Decoration */
#define EFIAPI
#define IN
#define OUT
#define OPTIONAL
#define CONST const

#define CAPSULE_FLAGS_PERSIST_ACROSS_RESET  0x00010000
#define CAPSULE_FLAGS_POPULATE_SYSTEM_TABLE 0x00020000
#define CAPSULE_FLAGS_INITIATE_RESET        0x00040000

//*******************************************************
// Memory Attribute Definitions
//*******************************************************
// These types can be "ORed" together as needed.
#define EFI_MEMORY_UC             0x0000000000000001
#define EFI_MEMORY_WC             0x0000000000000002
#define EFI_MEMORY_WT             0x0000000000000004
#define EFI_MEMORY_WB             0x0000000000000008
#define EFI_MEMORY_UCE            0x0000000000000010
#define EFI_MEMORY_WP             0x0000000000001000
#define EFI_MEMORY_RP             0x0000000000002000
#define EFI_MEMORY_XP             0x0000000000004000
#define EFI_MEMORY_NV             0x0000000000008000
#define EFI_MEMORY_MORE_RELIABLE  0x0000000000010000
#define EFI_MEMORY_RO             0x0000000000020000
#define EFI_MEMORY_RUNTIME        0x8000000000000000

//*******************************************************
// Memory Descriptor Version Number
//*******************************************************
#define EFI_MEMORY_DESCRIPTOR_VERSION 1


//*******************************************************
// Bit Definitions for EFI_TIME.Daylight. See below.
//*******************************************************
#define EFI_TIME_ADJUST_DAYLIGHT  0x01
#define EFI_TIME_IN_DAYLIGHT      0x02
//*******************************************************
// Value Definition for EFI_TIME.TimeZone. See below.
//*******************************************************
#define EFI_UNSPECIFIED_TIMEZONE 0x07FF

//ACPI
#define EFI_PNP_ID(ID) (UINT32)(((ID) << 16) | 0x41D0)
#define EISA_PNP_ID(ID) EFI_PNP_ID(ID)

#define EFI_SHIFT_STATE_VALID     0x80000000
#define EFI_RIGHT_SHIFT_PRESSED   0x00000001
#define EFI_LEFT_SHIFT_PRESSED    0x00000002
#define EFI_RIGHT_CONTROL_PRESSED 0x00000004
#define EFI_LEFT_CONTROL_PRESSED  0x00000008
#define EFI_RIGHT_ALT_PRESSED     0x00000010
#define EFI_LEFT_ALT_PRESSED      0x00000020
#define EFI_RIGHT_LOGO_PRESSED    0x00000040
#define EFI_LEFT_LOGO_PRESSED     0x00000080
#define EFI_MENU_KEY_PRESSED      0x00000100
#define EFI_SYS_REQ_PRESSED       0x00000200

#define EFI_TOGGLE_STATE_VALID  0x80
#define EFI_KEY_STATE_EXPOSED   0x40
#define EFI_SCROLL_LOCK_ACTIVE  0x01
#define EFI_NUM_LOCK_ACTIVE     0x02
#define EFI_CAPS_LOCK_ACTIVE    0x04

//*******************************************************
// Task Priority Levels
//*******************************************************
#define TPL_APPLICATION 4
#define TPL_CALLBACK    8
#define TPL_NOTIFY      16
#define TPL_HIGH_LEVEL  31

#define EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL  0x00000001
#define EFI_OPEN_PROTOCOL_GET_PROTOCOL        0x00000002
#define EFI_OPEN_PROTOCOL_TEST_PROTOCOL       0x00000004
#define EFI_OPEN_PROTOCOL_BY_CHILD_CONTROLLER 0x00000008
#define EFI_OPEN_PROTOCOL_BY_DRIVER           0x00000010
#define EFI_OPEN_PROTOCOL_EXCLUSIVE           0x00000020

/*--------------------------------------------------------------------------
**                     Type definitions
**--------------------------------------------------------------------------
*/

/* Common UEFI Data Types */
typedef VOID*                   EFI_HANDLE;
typedef VOID*                   EFI_EVENT;
typedef UINT64                  EFI_LBA;
typedef UINTN                   EFI_TPL;
typedef UINTN                   EFI_STATUS;




#define  EFI_SUCCESS  0
#define  EFI_WARN_UNKNOWN_GLYPH    1
#define  EFI_WARN_DELETE_FAILURE    2
#define  EFI_WARN_WRITE_FAILURE    3
#define  EFI_WARN_BUFFER_TOO_SMALL 4
#define  EFI_WARN_STALE_DATA 5

#define  EFI_LOAD_ERROR            0x80000001
#define  EFI_INVALID_PARAMETER     0x80000002
#define  EFI_UNSUPPORTED           0x80000003
#define  EFI_BAD_BUFFER_SIZE       0x80000004
#define  EFI_BUFFER_TOO_SMALL      0x80000005
#define  EFI_NOT_READY             0x80000006
#define  EFI_DEVICE_ERROR          0x80000007
#define  EFI_WRITE_PROTECTED       0x80000008
#define  EFI_OUT_OF_RESOURCES      0x80000009
#define  EFI_VOLUME_CORRUPTED      0x8000000A
#define  EFI_VOLUME_FULL           0x8000000B
#define  EFI_NO_MEDIA              0x8000000C
#define  EFI_MEDIA_CHANGED         0x8000000D
#define  EFI_NOT_FOUND             0x8000000E
#define  EFI_ACCESS_DENIED         0x8000000F
#define  EFI_NO_RESPONSE           0x80000010
#define  EFI_NO_MAPPING            0x80000011
#define  EFI_TIMEOUT               0x80000012
#define  EFI_NOT_STARTED           0x80000013
#define  EFI_ALREADY_STARTED       0x80000014
#define  EFI_ABORTED               0x80000015
#define  EFI_ICMP_ERROR            0x80000016
#define  EFI_TFTP_ERROR            0x80000017
#define  EFI_PROTOCOL_ERROR        0x80000018
#define  EFI_INCOMPATIBLE_VERSION  0x80000019
#define  EFI_SECURITY_VIOLATION    0x8000001A
#define  EFI_CRC_ERROR             0x8000001B
#define  EFI_END_OF_MEDIA          0x8000001C
#define  EFI_END_OF_FILE           0x8000001D
#define  EFI_INVALID_LANGUAGE      0x8000001E
#define  EFI_COMPROMISED_DATA      0x8000001F
#define  EFI_IP_ADDRESS_CONFLICT   0x80000020
#define  EFI_MODE_SWITCH_FAIL      0x80000021
#define  EFI_CRC_CHECK_FAIL        0x80000022
#define  EFI_MEM_QUEUE_EMPTY       0x80000023


/* RC Extended Errors */
#define EFI_ENGINE_BUSY           0x81000000
#define EFI_DEVICE_RESET_FAILED   0x81000001

#define MEM_SOURCE  9

//*******************************************************
// EFI_PXE_BASE_CODE_UDP_PORT
//*******************************************************
typedef UINT16 EFI_PXE_BASE_CODE_UDP_PORT;
//*******************************************************
// EFI_IPv4_ADDRESS and EFI_IPv6_ADDRESS
//*******************************************************
typedef struct {
  UINT8 Addr[4];
} EFI_IPv4_ADDRESS;
typedef struct {
  UINT8 Addr[16];
} EFI_IPv6_ADDRESS;
//*******************************************************
// EFI_IP_ADDRESS
//*******************************************************
typedef union {
UINT32 Addr[4];
  EFI_IPv4_ADDRESS v4;
  EFI_IPv6_ADDRESS v6;
} EFI_IP_ADDRESS;
//*******************************************************
// EFI_MAC_ADDRESS
//*******************************************************
typedef struct {
  UINT8 Addr[32];
} EFI_MAC_ADDRESS;

//*******************************************************
// EFI_KEY_TOGGLE_STATE
//*******************************************************
typedef UINT8 EFI_KEY_TOGGLE_STATE;

//*******************************************************
//EFI_PHYSICAL_ADDRESS
//*******************************************************
typedef UINT64 EFI_PHYSICAL_ADDRESS;

//*******************************************************
//EFI_VIRTUAL_ADDRESS
//*******************************************************
typedef UINT64 EFI_VIRTUAL_ADDRESS;

//*******************************************************
//EFI_GUID
//*******************************************************
typedef struct {
  UINT32 Data1;
  UINT16 Data2;
  UINT16 Data3;
  UINT8 Data4[8];
} EFI_GUID;
//*******************************************************
//EFI_INTERFACE_TYPE
//*******************************************************
typedef enum {
  EFI_NATIVE_INTERFACE
} EFI_INTERFACE_TYPE;

typedef struct
{
  UINT64 Signature;   /* Identifies type of table */
  UINT32 Revision;    /* Revision of EFI Spec to which the table conforms. */
  UINT32 HeaderSize;  /* Size in bytes of the entire table including EFI_TABLE_HEADER */
  UINT32 CRC32;       /* 32-bit CRC for the entire table */
  UINT32 Reserved;    /* Reserved field that must be set to 0 */
} EFI_TABLE_HEADER;

typedef struct
{
  EFI_GUID VendorGuid;
  VOID *VendorTable;
} EFI_CONFIGURATION_TABLE;

typedef struct
{
  UINT32 Version;
  UINT32 Length;
  UINT64 MemoryProtectionAttribute;
} EFI_PROPERTIES_TABLE;




//*******************************************************
// EFI_RESET_TYPE
//*******************************************************
typedef enum {
  EfiResetCold,
  EfiResetWarm,
  EfiResetShutdown,
  EfiResetPlatformSpecific
} EFI_RESET_TYPE;

typedef struct {
  EFI_GUID CapsuleGuid;
  UINT32 HeaderSize;
  UINT32 Flags;
  UINT32 CapsuleImageSize;
} EFI_CAPSULE_HEADER;

//*******************************************************
//EFI_MEMORY_DESCRIPTOR
//*******************************************************
typedef struct {
  UINT32 Type;
  EFI_PHYSICAL_ADDRESS PhysicalStart;
  EFI_VIRTUAL_ADDRESS VirtualStart;
  UINT64 NumberOfPages;
  UINT64 Attribute;
} EFI_MEMORY_DESCRIPTOR;

//*******************************************************
//EFI_TIME
//*******************************************************
// This represents the current time information
typedef struct {
  UINT16 Year; // 1900 – 9999
  UINT8 Month; // 1 – 12
  UINT8 Day; // 1 – 31
  UINT8 Hour; // 0 – 23
  UINT8 Minute; // 0 – 59
  UINT8 Second; // 0 – 59
  UINT8 Pad1;
  UINT32 Nanosecond; // 0 – 999,999,999
  INT16 TimeZone; // -1440 to 1440 or 2047
  UINT8 Daylight;
  UINT8 Pad2;
} EFI_TIME;

//*******************************************************
// EFI_TIME_CAPABILITIES
//*******************************************************
// This provides the capabilities of the
// real time clock device as exposed through the EFI interfaces.
typedef struct {
  UINT32 Resolution;
  UINT32 Accuracy;
  BOOLEAN SetsToZero;
} EFI_TIME_CAPABILITIES;


//*******************************************************
// EFI_LOCATE_SEARCH_TYPE
//*******************************************************
typedef enum {
  AllHandles,
  ByRegisterNotify,
  ByProtocol
} EFI_LOCATE_SEARCH_TYPE;

//*******************************************************
// EFI_DEVICE_PATH_PROTOCOL
//*******************************************************
typedef struct _EFI_DEVICE_PATH_PROTOCOL {
  UINT8 Type;
  UINT8 SubType;
  UINT8 Length[2];
} EFI_DEVICE_PATH_PROTOCOL;

typedef struct {
  EFI_HANDLE AgentHandle;
  EFI_HANDLE ControllerHandle;
  UINT32 Attributes;
  UINT32 OpenCount;
} EFI_OPEN_PROTOCOL_INFORMATION_ENTRY;

//*******************************************************
//EFI_ALLOCATE_TYPE
//*******************************************************
// These types are discussed in the “Description” section below.
typedef enum {
  AllocateAnyPages,
  AllocateMaxAddress,
  AllocateAddress,
  MaxAllocateType
} EFI_ALLOCATE_TYPE;
//*******************************************************
//EFI_MEMORY_TYPE
//*******************************************************
// These type values are discussed in Table 25 and Table 26.
typedef enum {
  EfiReservedMemoryType,
  EfiLoaderCode,
  EfiLoaderData,
  EfiBootServicesCode,
  EfiBootServicesData,
  EfiRuntimeServicesCode,
  EfiRuntimeServicesData,
  EfiConventionalMemory,
  EfiUnusableMemory,
  EfiACPIReclaimMemory,
  EfiACPIMemoryNVS,
  EfiMemoryMappedIO,
  EfiMemoryMappedIOPortSpace,
  EfiPalCode,
  EfiPersistentMemory,
  EfiMaxMemoryType,
  EfiA717SharedMemory,
  EfiEndOfMemory
} EFI_MEMORY_TYPE;

typedef enum
{
  EFI_SERVICES_EVT_TIMER = 0,
  EFI_SERVICES_EVT_RUNTIME,
  EFI_SERVICES_EVT_NOTIFY_WAIT,
  EFI_SERVICES_EVT_NOTIFY_SIGNAL,
  EFI_SERVICES_EVT_EXIT_BOOT_SERVICES,
  EFI_SERVICES_EVT_SIGNAL_VIRTUAL_ADDRESS_CHANGE,
  EFI_SERVICES_EVT_LIM
}EFI_SERVICES_EVENT_TYPE;

typedef struct {
  EFI_MEMORY_TYPE Type;
  EFI_PHYSICAL_ADDRESS *HeapStartAddress;
  EFI_PHYSICAL_ADDRESS *CurrentHeapPointer;
  UINTN Size;
} MEMORY_TYPE_HEAP;

/*----------------------------------------------------------------------
**                     Console Support
**----------------------------------------------------------------------
*/

/*----------------------------------------------------------------------
**                     Simple Text Input Protocol
**----------------------------------------------------------------------
*/
typedef union {
  struct {
    UINT32 Revision : 8;
    UINT32 ShiftPressed : 1;
    UINT32 ControlPressed : 1;
    UINT32 AltPressed : 1;
    UINT32 LogoPressed : 1;
    UINT32 MenuPressed : 1;
    UINT32 SysReqPressed : 1;
    UINT32 Reserved : 16;
    UINT32 InputKeyCount : 2;
  } Options;
  UINT32 PackedValue;
} EFI_BOOT_KEY_DATA;

//*******************************************************
// EFI_INPUT_KEY
//*******************************************************
typedef struct {
  UINT16 ScanCode;
  CHAR16 UnicodeChar;
} EFI_INPUT_KEY;

typedef struct _EFI_KEY_OPTION {
  EFI_BOOT_KEY_DATA KeyData;
  UINT32 BootOptionCrc;
  UINT16 BootOption;
  // EFI_INPUT_KEY Keys[];
} EFI_KEY_OPTION;

//*******************************************************
// EFI_KEY_STATE
//*******************************************************
//
// Any Shift or Toggle State that is valid should have
// high order bit set.
//
typedef struct EFI_KEY_STATE {
  UINT32 KeyShiftState;
  EFI_KEY_TOGGLE_STATE KeyToggleState;
} EFI_KEY_STATE;

//*******************************************************
// EFI_KEY_DATA
//*******************************************************

typedef struct _EFI_SIMPLE_TEXT_INPUT_PROTOCOL EFI_SIMPLE_TEXT_INPUT_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *EFI_INPUT_RESET) (
  IN EFI_SIMPLE_TEXT_INPUT_PROTOCOL *This,
  IN BOOLEAN ExtendedVerification
);

typedef
EFI_STATUS
(EFIAPI *EFI_INPUT_READ_KEY) (
  IN EFI_SIMPLE_TEXT_INPUT_PROTOCOL *This,
  OUT EFI_INPUT_KEY *Key
);

typedef struct {
  EFI_INPUT_KEY Key;
  EFI_KEY_STATE KeyState;
} EFI_KEY_DATA;

struct _EFI_SIMPLE_TEXT_INPUT_PROTOCOL {
  EFI_INPUT_RESET Reset;
  EFI_INPUT_READ_KEY ReadKeyStroke;
  EFI_EVENT WaitForKey;
};




/*----------------------------------------------------------------------
**                     Simple Text Output Protocol
**----------------------------------------------------------------------
*/

typedef struct _EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_RESET) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN BOOLEAN ExtendedVerification
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_STRING) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN const CHAR16 *String
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_TEST_STRING) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN CHAR16 *String
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_QUERY_MODE) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN UINTN ModeNumber,
  OUT UINTN *Columns,
  OUT UINTN *Rows
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_SET_MODE) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN UINTN ModeNumber
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_SET_ATTRIBUTE) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN UINTN Attribute
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_CLEAR_SCREEN) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_SET_CURSOR_POSITION) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN UINTN Column,
  IN UINTN Row
);

typedef
EFI_STATUS
(EFIAPI *EFI_TEXT_ENABLE_CURSOR) (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *This,
  IN BOOLEAN Visible
);



//*******************************************************
// SIMPLE_TEXT_OUTPUT_MODE
//*******************************************************
typedef struct {
  INT32 MaxMode;
  // current settings
  INT32 Mode;
  INT32 Attribute;
  INT32 CursorColumn;
  INT32 CursorRow;
  BOOLEAN CursorVisible;
} SIMPLE_TEXT_OUTPUT_MODE;

struct _EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL {
  EFI_TEXT_RESET Reset;
  EFI_TEXT_STRING OutputString;
  EFI_TEXT_TEST_STRING TestString;
  EFI_TEXT_QUERY_MODE QueryMode;
  EFI_TEXT_SET_MODE SetMode;
  EFI_TEXT_SET_ATTRIBUTE SetAttribute;
  EFI_TEXT_CLEAR_SCREEN ClearScreen;
  EFI_TEXT_SET_CURSOR_POSITION SetCursorPosition;
  EFI_TEXT_ENABLE_CURSOR EnableCursor;
  SIMPLE_TEXT_OUTPUT_MODE *Mode;
};

/* Timer Delay Enumeration */
typedef enum
{
  TimerCancel,
  TimerPeriodic,
  TimerRelative
} EFI_TIMER_DELAY;


//*******************************************************
// EFI_EVENT_NOTIFY
//*******************************************************
typedef
VOID
(EFIAPI *EFI_EVENT_NOTIFY) (
  IN EFI_EVENT Event,
  IN VOID *Context
);
/* Function pointer prototypes */
typedef
EFI_STATUS
(EFIAPI *EFI_CREATE_EVENT) (
  IN UINT32 Type,
  IN EFI_TPL NotifyTpl,
  IN EFI_EVENT_NOTIFY NotifyFunction OPTIONAL,
  IN VOID *NotifyContext OPTIONAL,
  OUT EFI_EVENT *Event
);

typedef
EFI_STATUS
(EFIAPI *EFI_CREATE_EVENT_EX) (
  IN UINT32 Type,
  IN EFI_TPL NotifyTpl,
  IN EFI_EVENT_NOTIFY NotifyFunction OPTIONAL,
  IN CONST VOID *NotifyContext OPTIONAL,
  IN CONST EFI_GUID *EventGroup OPTIONAL,
  OUT EFI_EVENT *Event
);

typedef
EFI_STATUS
(EFIAPI *EFI_CLOSE_EVENT) (
  IN EFI_EVENT Event
);

typedef
EFI_STATUS
(EFIAPI *EFI_SIGNAL_EVENT) (
  IN EFI_EVENT Event
);

typedef
EFI_STATUS
(EFIAPI *EFI_WAIT_FOR_EVENT) (
  IN UINTN NumberOfEvents,
  IN EFI_EVENT *Event,
  OUT UINTN *Index
);

typedef
EFI_STATUS
(EFIAPI *EFI_CHECK_EVENT) (
  IN EFI_EVENT Event
);

typedef
EFI_STATUS
(EFIAPI *EFI_SET_TIMER) (
  IN EFI_EVENT Event,
  IN EFI_TIMER_DELAY Type,
  IN UINT64 TriggerTime
);

typedef
EFI_TPL
(EFIAPI *EFI_RAISE_TPL) (
  IN EFI_TPL NewTpl
);

typedef
VOID
(EFIAPI *EFI_RESTORE_TPL) (
  IN EFI_TPL OldTpl
);

typedef
EFI_STATUS
(EFIAPI *EFI_ALLOCATE_PAGES) (
  IN EFI_ALLOCATE_TYPE Type,
  IN EFI_MEMORY_TYPE MemoryType,
  IN UINTN Pages,
  IN OUT EFI_PHYSICAL_ADDRESS *Memory
);

typedef
EFI_STATUS
(EFIAPI *EFI_FREE_PAGES) (
  IN EFI_PHYSICAL_ADDRESS Memory,
  IN UINTN Pages
);

typedef
EFI_STATUS
(EFIAPI *EFI_GET_MEMORY_MAP) (
  IN OUT UINTN *MemoryMapSize,
  IN OUT EFI_MEMORY_DESCRIPTOR *MemoryMap,
  OUT UINTN *MapKey,
  OUT UINTN *DescriptorSize,
  OUT UINT32 *DescriptorVersion
);

typedef
EFI_STATUS
(EFIAPI *EFI_ALLOCATE_POOL) (
  IN EFI_MEMORY_TYPE PoolType,
  IN UINTN Size,
  OUT VOID **Buffer
);

typedef
EFI_STATUS
(EFIAPI *EFI_FREE_POOL) (
  IN VOID *Buffer
);

typedef
EFI_STATUS
(EFIAPI *EFI_SIGNAL_EVENT_TYPE)(
  IN CONST EFI_SERVICES_EVENT_TYPE EventType
);

/*----------------------------------------------------------------------
 *                    Protocol Handler Services
 *----------------------------------------------------------------------
 */
typedef
EFI_STATUS
(EFIAPI *EFI_INSTALL_PROTOCOL_INTERFACE) (
  IN OUT EFI_HANDLE *Handle,
  IN EFI_GUID *Protocol,
  IN EFI_INTERFACE_TYPE InterfaceType,
  IN VOID *Interface
);

typedef
EFI_STATUS
(EFIAPI *EFI_UNINSTALL_PROTOCOL_INTERFACE) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  IN VOID *Interface
);

typedef
EFI_STATUS
(EFIAPI *EFI_REINSTALL_PROTOCOL_INTERFACE) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  IN VOID *OldInterface,
  IN VOID *NewInterface
);

typedef
EFI_STATUS
(EFIAPI *EFI_REGISTER_PROTOCOL_NOTIFY) (
  IN EFI_GUID *Protocol,
  IN EFI_EVENT Event,
  OUT VOID **Registration
);

typedef
EFI_STATUS
(EFIAPI *EFI_LOCATE_HANDLE) (
  IN EFI_LOCATE_SEARCH_TYPE SearchType,
  IN EFI_GUID *Protocol OPTIONAL,
  IN VOID *SearchKey OPTIONAL,
  IN OUT UINTN *BufferSize,
  OUT EFI_HANDLE *Buffer
);

typedef
EFI_STATUS
(EFIAPI *EFI_HANDLE_PROTOCOL) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  OUT VOID **Interface
);

typedef
EFI_STATUS
(EFIAPI *EFI_LOCATE_DEVICE_PATH) (
  IN EFI_GUID *Protocol,
  IN OUT EFI_DEVICE_PATH_PROTOCOL **DevicePath,
  OUT EFI_HANDLE *Device
);

typedef
EFI_STATUS
(EFIAPI *EFI_OPEN_PROTOCOL) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  OUT VOID **Interface OPTIONAL,
  IN EFI_HANDLE AgentHandle,
  IN EFI_HANDLE ControllerHandle,
  IN UINT32 Attributes
);

typedef
EFI_STATUS
(EFIAPI *EFI_CLOSE_PROTOCOL) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  IN EFI_HANDLE AgentHandle,
  IN EFI_HANDLE ControllerHandle
);

typedef
EFI_STATUS
(EFIAPI *EFI_OPEN_PROTOCOL_INFORMATION) (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  OUT EFI_OPEN_PROTOCOL_INFORMATION_ENTRY **EntryBuffer,
  OUT UINTN *EntryCount
);

typedef
EFI_STATUS
(EFIAPI *EFI_CONNECT_CONTROLLER) (
  IN EFI_HANDLE ControllerHandle,
  IN EFI_HANDLE *DriverImageHandle OPTIONAL,
  IN EFI_DEVICE_PATH_PROTOCOL *RemainingDevicePath OPTIONAL,
  IN BOOLEAN Recursive
);

typedef
EFI_STATUS
(EFIAPI *EFI_DISCONNECT_CONTROLLER) (
  IN EFI_HANDLE ControllerHandle,
  IN EFI_HANDLE DriverImageHandle OPTIONAL,
  IN EFI_HANDLE ChildHandle OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_PROTOCOLS_PER_HANDLE) (
  IN EFI_HANDLE Handle,
  OUT EFI_GUID ***ProtocolBuffer,
  OUT UINTN *ProtocolBufferCount
);

typedef
EFI_STATUS
(EFIAPI *EFI_LOCATE_HANDLE_BUFFER) (
  IN EFI_LOCATE_SEARCH_TYPE SearchType,
  IN EFI_GUID *Protocol OPTIONAL,
  IN VOID *SearchKey OPTIONAL,
  IN OUT UINTN *NoHandles,
  OUT EFI_HANDLE **Buffer
);

typedef
EFI_STATUS
(EFIAPI *EFI_LOCATE_PROTOCOL) (
  IN EFI_GUID *Protocol,
  IN VOID *Registration OPTIONAL,
  OUT VOID **Interface
);

typedef
EFI_STATUS
(EFIAPI *EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES) (
  IN OUT EFI_HANDLE *Handle,
  ...
);

typedef
EFI_STATUS
(EFIAPI *EFI_UNINSTALL_MULTIPLE_PROTOCOL_INTERFACES) (
  IN EFI_HANDLE Handle,
  ...
);

/*----------------------------------------------------------------------
 *                    Image Services
 *----------------------------------------------------------------------
 */

typedef
EFI_STATUS
(EFIAPI *EFI_IMAGE_LOAD) (
  IN BOOLEAN BootPolicy,
  IN EFI_HANDLE ParentImageHandle,
  IN EFI_DEVICE_PATH_PROTOCOL *DevicePath,
  IN VOID *SourceBuffer OPTIONAL,
  IN UINTN SourceSize,
  OUT EFI_HANDLE *ImageHandle
);

typedef
EFI_STATUS
(EFIAPI *EFI_IMAGE_START) (
  IN EFI_HANDLE ImageHandle,
  OUT UINTN *ExitDataSize,
  OUT CHAR16 **ExitData OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_IMAGE_UNLOAD) (
  IN EFI_HANDLE ImageHandle
);


typedef
EFI_STATUS
(EFIAPI *EFI_EXIT) (
  IN EFI_HANDLE ImageHandle,
  IN EFI_STATUS ExitStatus,
  IN UINTN ExitDataSize,
  IN CHAR16 *ExitData OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_EXIT_BOOT_SERVICES) (
  IN EFI_HANDLE ImageHandle,
  IN UINTN MapKey
);

/*----------------------------------------------------------------------
 *                    Miscellaneous Boot Services
 *----------------------------------------------------------------------
 */

typedef
EFI_STATUS
(EFIAPI *EFI_SET_WATCHDOG_TIMER) (
  IN UINTN Timeout,
  IN UINT64 WatchdogCode,
  IN UINTN DataSize,
  IN CHAR16 *WatchdogData OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_STALL) (
  IN UINTN Microseconds
);

typedef
VOID
(EFIAPI *EFI_COPY_MEM) (
  IN VOID *Destination,
  IN VOID *Source,
  IN UINTN Length
);

typedef
VOID
(EFIAPI *EFI_SET_MEM) (
  IN VOID *Buffer,
  IN UINTN Size,
  IN UINT8 Value
);

typedef
EFI_STATUS
(EFIAPI *EFI_GET_NEXT_MONOTONIC_COUNT) (
  OUT UINT64 *Count
);

typedef
EFI_STATUS
(EFIAPI *EFI_INSTALL_CONFIGURATION_TABLE) (
  IN EFI_GUID *Guid,
  IN VOID *Table
);

typedef
EFI_STATUS
(EFIAPI *EFI_CALCULATE_CRC32) (
  IN VOID *Data,
  IN UINTN DataSize,
  OUT UINT32 *Crc32
);

/*----------------------------------------------------------------------
 *                    RC Boot Services
 *----------------------------------------------------------------------
 */

typedef
INT32
(EFIAPI *EFI_COMPARE_MEM) (
  IN const VOID *str1,
  IN const VOID *str2,
  IN UINTN Length
);

/*----------------------------------------------------------------------
 *                    Runtime Services
 *----------------------------------------------------------------------
 */

/*----------------------------------------------------------------------
 *                    Variable Services
 *----------------------------------------------------------------------
 */

typedef
EFI_STATUS
(EFIAPI *EFI_GET_VARIABLE) (
  IN CHAR16 *VariableName,
  IN EFI_GUID *VendorGuid,
  OUT UINT32 *Attributes OPTIONAL,
  IN OUT UINTN *DataSize,
  OUT VOID *Data
);

typedef
EFI_STATUS
(EFIAPI *EFI_GET_NEXT_VARIABLE_NAME) (
  IN OUT UINTN *VariableNameSize,
  IN OUT CHAR16 *VariableName,
  IN OUT EFI_GUID *VendorGuid
);

typedef
EFI_STATUS
(EFIAPI *EFI_SET_VARIABLE) (
  IN CHAR16 *VariableName,
  IN EFI_GUID *VendorGuid,
  IN UINT32 Attributes,
  IN UINTN DataSize,
  IN VOID *Data
);

typedef
EFI_STATUS
(EFIAPI *EFI_QUERY_VARIABLE_INFO) (
  IN UINT32 Attributes,
  OUT UINT64 *MaximumVariableStorageSize,
  OUT UINT64 *RemainingVariableStorageSize,
  OUT UINT64 *MaximumVariableSize
);


/*----------------------------------------------------------------------
 *                    Time Services
 *----------------------------------------------------------------------
 */

typedef
EFI_STATUS
(EFIAPI *EFI_GET_TIME) (
  OUT EFI_TIME *Time,
  OUT EFI_TIME_CAPABILITIES *Capabilities OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_SET_TIME) (
  IN EFI_TIME *Time
);

typedef
EFI_STATUS
(EFIAPI *EFI_GET_WAKEUP_TIME) (
  OUT BOOLEAN *Enabled,
  OUT BOOLEAN *Pending,
  OUT EFI_TIME *Time
);

typedef
EFI_STATUS
(EFIAPI *EFI_SET_WAKEUP_TIME) (
  IN BOOLEAN Enable,
  IN EFI_TIME *Time OPTIONAL
);

/*----------------------------------------------------------------------
 *                    Virtual Memory Services
 *----------------------------------------------------------------------
 */

typedef
EFI_STATUS
(EFIAPI *EFI_SET_VIRTUAL_ADDRESS_MAP) (
  IN UINTN MemoryMapSize,
  IN UINTN DescriptorSize,
  IN UINT32 DescriptorVersion,
  IN EFI_MEMORY_DESCRIPTOR *VirtualMap
);

typedef
EFI_STATUS
(EFIAPI *EFI_CONVERT_POINTER) (
  IN UINTN DebugDisposition,
  IN VOID **Address
);

/*----------------------------------------------------------------------
 *                    Miscellaneous Runtime Services
 *----------------------------------------------------------------------
 */

typedef
VOID
(EFIAPI *EFI_RESET_SYSTEM) (
  IN EFI_RESET_TYPE ResetType,
  IN EFI_STATUS ResetStatus,
  IN UINTN DataSize,
  IN VOID *ResetData OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_GET_NEXT_HIGH_MONO_COUNT) (
  OUT UINT32 *HighCount
);

typedef
EFI_STATUS
(EFIAPI *EFI_UPDATE_CAPSULE) (
  IN EFI_CAPSULE_HEADER **CapsuleHeaderArray,
  IN UINTN CapsuleCount,
  IN EFI_PHYSICAL_ADDRESS ScatterGatherList OPTIONAL
);

typedef
EFI_STATUS
(EFIAPI *EFI_QUERY_CAPSULE_CAPABILITIES) (
  IN EFI_CAPSULE_HEADER **CapsuleHeaderArray,
  IN UINTN CapsuleCount,
  OUT UINT64 *MaximumCapsuleSize,
  OUT EFI_RESET_TYPE *ResetType
);


/* Boot Services */
typedef struct
{
  EFI_TABLE_HEADER Hdr;

  /* Task Priority Services */
  EFI_RAISE_TPL   RaiseTPL;   /* EFI 1.0+ */
  EFI_RESTORE_TPL RestoreTPL; /* EFI 1.0+ */

  /* Memory Services */
  EFI_ALLOCATE_PAGES  AllocatePages;  /* EFI 1.0+ */
  EFI_FREE_PAGES      FreePages;      /* EFI 1.0+ */
  EFI_GET_MEMORY_MAP  GetMemoryMap;   /* EFI 1.0+ */
  EFI_ALLOCATE_POOL   AllocatePool;   /* EFI 1.0+ */
  EFI_FREE_POOL       FreePool;       /* EFI 1.0+ */

  /* Event & Timer Services */
  EFI_CREATE_EVENT    CreateEvent;  /* EFI 1.0+ */
  EFI_SET_TIMER       SetTimer;     /* EFI 1.0+ */
  EFI_WAIT_FOR_EVENT  WaitForEvent; /* EFI 1.0+ */
  EFI_SIGNAL_EVENT    SignalEvent;  /* EFI 1.0+ */
  EFI_CLOSE_EVENT     CloseEvent;   /* EFI 1.0+ */
  EFI_CHECK_EVENT     CheckEvent;   /* EFI 1.0+ */

  /* Protocol Handler Services */
  EFI_INSTALL_PROTOCOL_INTERFACE    InstallProtocolInterface;   /* EFI 1.0+ */
  EFI_REINSTALL_PROTOCOL_INTERFACE  ReinstallProtocolInterface; /* EFI 1.0+ */
  EFI_UNINSTALL_PROTOCOL_INTERFACE  UninstallProtocolInterface; /* EFI 1.0+ */
  EFI_HANDLE_PROTOCOL               HandleProtocol;             /* EFI 1.0+ */
  VOID*                             Reserved;                   /* EFI 1.0+ */
  EFI_REGISTER_PROTOCOL_NOTIFY      RegisterProtocolNotify;     /* EFI 1.0+ */
  EFI_LOCATE_HANDLE                 LocateHandle;               /* EFI 1.0+ */
  EFI_LOCATE_DEVICE_PATH            LocateDevicePath;           /* EFI 1.0+ */
  EFI_INSTALL_CONFIGURATION_TABLE   InstallConfigurationTable;  /* EFI 1.0+ */

  /* Image Services */
  EFI_IMAGE_LOAD          LoadImage;        /* EFI 1.0+ */
  EFI_IMAGE_START         StartImage;       /* EFI 1.0+ */
  EFI_EXIT                Exit;             /* EFI 1.0+ */
  EFI_IMAGE_UNLOAD        UnloadImage;      /* EFI 1.0+ */
  EFI_EXIT_BOOT_SERVICES  ExitBootServices; /* EFI 1.0+ */

  /* Miscellaneous Services */
  EFI_GET_NEXT_MONOTONIC_COUNT  GetNextMonotonicCount;  /* EFI 1.0+ */
  EFI_STALL                     Stall;                  /* EFI 1.0+ */
  EFI_SET_WATCHDOG_TIMER        SetWatchdogTimer;       /* EFI 1.0+ */

  /* DriverSupport Services */
  EFI_CONNECT_CONTROLLER    ConnectController;    /* EFI 1.1 */
  EFI_DISCONNECT_CONTROLLER DisconnectController; /* EFI 1.1+ */

  /* Open and Close Protocol Services */
  EFI_OPEN_PROTOCOL             OpenProtocol;             /* EFI 1.1+ */
  EFI_CLOSE_PROTOCOL            CloseProtocol;            /* EFI 1.1+ */
  EFI_OPEN_PROTOCOL_INFORMATION OpenProtocolInformation;  /* EFI 1.1+ */

  /* Library Services */
  EFI_PROTOCOLS_PER_HANDLE                    ProtocolsPerHandle;                   /* EFI 1.1+ */
  EFI_LOCATE_HANDLE_BUFFER                    LocateHandleBuffer;                   /* EFI 1.1+ */
  EFI_LOCATE_PROTOCOL                         LocateProtocol;                       /* EFI 1.1+ */
  EFI_INSTALL_MULTIPLE_PROTOCOL_INTERFACES    InstallMultipleProtocolInterfaces;    /* EFI 1.1+ */
  EFI_UNINSTALL_MULTIPLE_PROTOCOL_INTERFACES  UninstallMultipleProtocolInterfaces;  /* EFI 1.1+ */

  /* 32-bit CRC Services */
  EFI_CALCULATE_CRC32 CalculateCrc32; /* EFI 1.1+ */

  /* Miscellaneous Services */
  EFI_COPY_MEM        CopyMem;        /* EFI 1.1+ */
  EFI_SET_MEM         SetMem;         /* EFI 1.1+ */
  EFI_CREATE_EVENT_EX CreateEventEx;  /* UEFI 2.0+ */

  /* RC Services */
  EFI_COMPARE_MEM CompareMem;
  EFI_SIGNAL_EVENT_TYPE SignalEventType;
} EFI_BOOT_SERVICES;

/* Runtime Services */
typedef struct
{
  EFI_TABLE_HEADER Hdr;

  /* Time Services */
  EFI_GET_TIME        GetTime;
  EFI_SET_TIME        SetTime;
  EFI_GET_WAKEUP_TIME GetWakeupTime;
  EFI_SET_WAKEUP_TIME SetWakeupTime;

  /* Virtual Memory Services */
  EFI_SET_VIRTUAL_ADDRESS_MAP SetVirtualAddressMap;
  EFI_CONVERT_POINTER         ConvertPointer;

  /* Variable Services */
  EFI_GET_VARIABLE            GetVariable;
  EFI_GET_NEXT_VARIABLE_NAME  GetNextVariableName;
  EFI_SET_VARIABLE            SetVariable;

  /* Miscellaneous Services */
  EFI_GET_NEXT_HIGH_MONO_COUNT  GetNextHighMonotonicCount;
  EFI_RESET_SYSTEM              ResetSystem;

  /* UEFI 2.0 Capsule Services */
  EFI_UPDATE_CAPSULE              UpdateCapsule;
  EFI_QUERY_CAPSULE_CAPABILITIES  QueryCapsuleCapabilities;

  /* Miscellaneous UEFI 2.0 Service */
  EFI_QUERY_VARIABLE_INFO QueryVariableInfo;
} EFI_RUNTIME_SERVICES;

/* System Table */
typedef struct
{
  EFI_TABLE_HEADER                Hdr;
  CHAR16                          *FirmwareVendor;
  UINT32                          FirmwareRevision;
  EFI_HANDLE                      ConsoleInHandle;
  EFI_SIMPLE_TEXT_INPUT_PROTOCOL  *ConIn;
  EFI_HANDLE                      ConsoleOutHandle;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *ConOut;
  EFI_HANDLE                      StandardErrorHandle;
  EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL *StdErr;
  EFI_RUNTIME_SERVICES            *RuntimeServices;
  EFI_BOOT_SERVICES               *BootServices;
  UINTN                           NumberOfTableEntries;
  EFI_CONFIGURATION_TABLE         *ConfigurationTable;

  /* Extended variables */
  MEMORY_TYPE_HEAP                 HobMemSource[MEM_SOURCE];
  EFI_HANDLE                       CoreImageHandle;
  EFI_HANDLE                       ExecutingImageHandle;
  EFI_HANDLE                       ImageListHead;
  EFI_HANDLE                      *RootFieldFs;
  EFI_HANDLE                      *RootRamFs;
  EFI_HANDLE                      *RootTftpFs;
  UINT64                           SystemCounterTicks;
  VOID                            *ElfLoadAddress;
  UINT64                           CoreSharedA717StartAddr;
} EFI_SYSTEM_TABLE;

typedef
EFI_STATUS
(EFIAPI *EFI_IMAGE_ENTRY_POINT) (
  IN EFI_HANDLE ImageHandle,
  IN EFI_SYSTEM_TABLE *SystemTable
);

extern EFI_SYSTEM_TABLE      *gST;
extern EFI_BOOT_SERVICES     *gBS;
extern EFI_RUNTIME_SERVICES  *gRT;

#endif /* EFI_H */
