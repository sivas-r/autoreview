/*--------------------------------------------------------------------------
**
** Copyright:  Copyright 2016 Rockwell Collins. All rights reserved.
**             Rockwell Collins Proprietary Information.
**
** File:  platform_typedef.h
**
** Description:  .
**
**--------------------------------------------------------------------------
*/

#ifndef PLATFORM_TYPEDEF_H
#define PLATFORM_TYPEDEF_H


/*--------------------------------------------------------------------------
**                                     Include Files
**--------------------------------------------------------------------------
*/
#ifndef true
#define true					(1)
#endif 

#ifndef false
#define false					(0)
#endif

typedef signed int              INTN;
typedef unsigned int            UINTN;
typedef signed char             INT8;
typedef unsigned char           UINT8;
typedef signed short int        INT16;
typedef unsigned short int      UINT16;
typedef signed int              INT32;
typedef unsigned int            UINT32;
typedef signed long long int    INT64;
typedef unsigned long long int  UINT64;
typedef INT8                    CHAR8;
typedef UINT16                  CHAR16;
typedef unsigned int            uintptr_t;
#ifndef __cplusplus
#ifndef bool
typedef UINT8                   bool;
#endif
#endif
typedef void                    VOID;
typedef bool                    BOOLEAN;
typedef char                    CHAR;

typedef signed char				int8_t;
typedef signed short int		int16_t;
typedef signed int				int32_t;
typedef signed long long int	int64_t;

typedef unsigned char			uint8_t;
typedef unsigned short int		uint16_t;
typedef UINT32					uint32_t;
typedef unsigned long long int	uint64_t;

#define INTN_MIN             (INTN)0xffffffff
#define INTN_MAX             (INTN)0x7fffffff
#define UINTN_MAX            (UINTN)0xffffffff
#define UINT32_MAX           UINTN_MAX

#ifndef __size_t
#ifndef WIN32
#ifdef __cplusplus
typedef unsigned int       size_t;
#else
typedef long unsigned int       size_t;
#endif /* __cplusplus */
#endif /* WIN32 */
#define __size_t
#endif /* __size_t */

#ifndef __cplusplus
typedef unsigned short int      wchar_t;
#endif /* __cplusplus */

#ifndef NULL
#define NULL 0
#endif 

typedef union
{
  struct _QWORD_TYPE
  {
    UINT32 lower;
    UINT32 upper;
  }dword;
  UINT64 qwvalue;
}QWORD_BUFFER;

typedef union
{
  struct _DWORD_TYPE
  {
    UINT16 lower;
    UINT16 upper;
  }word;
  UINT32 dwvalue;
}DWORD_BUFFER;

typedef union
{
  struct _WORD_TYPE
  {
    UINT8 lower;
    UINT8 upper;
  }byte;
  UINT16 wvalue;
}WORD_BUFFER;

typedef union
{
  struct _PTR_TYPE
  {
    VOID *ptr;
    UINT32 reserved;
  }ptr;
  UINT64 ptr64;
}PTR_BUFFER;

#ifndef WIN32
#define UINT32_C(value)       value ## u
#endif

#endif /* PLATFORM_TYPEDEF_H */
