/*--------------------------------------------------------------------------
**
** Copyright 2016 Rockwell Collins, Inc.  All rights reserved.
**
** File:  efi_spi_protocol.h
**
** Description:
** Definitions for the uefi spi protocol functions.
**
**--------------------------------------------------------------------------
*/
#ifndef EFI_LOADED_IMAGE_PROTOCOL_H
#define EFI_LOADED_IMAGE_PROTOCOL_H

/*--------------------------------------------------------------------------
**                     Include Files
**--------------------------------------------------------------------------
*/

#include "efi.h"

/*--------------------------------------------------------------------------
**                     Literal Declarations & Macros
**--------------------------------------------------------------------------
*/

#define EFI_LOADED_IMAGE_PROTOCOL_GUID\
  {0x5B1B31A1,0x9562,0x11d2,\
  {0x8E,0x3F,0x00,0xA0,0xC9,0x69,0x72,0x3B}}

#define EFI_LOADED_IMAGE_DEVICE_PATH_PROTOCOL_GUID \
  {0xbc62157e,0x3e33,0x4fec,\
  {0x99,0x20,0x2d,0x3b,0x36,0xd7,0x50,0xdf}}

#define EFI_LOADED_IMAGE_PROTOCOL_REVISION 0x1000

/* 4 sections, text, rodata, data, and bss */
#define IMAGE_SECTION_NUMBER  4

typedef struct {
  UINT32            Revision;
  EFI_HANDLE        ParentHandle;
  EFI_SYSTEM_TABLE  *SystemTable;
  // Source location of the image
  EFI_HANDLE                DeviceHandle;
  EFI_DEVICE_PATH_PROTOCOL  *FilePath;
  VOID                      *Reserved;
  // Image�s load options
  UINT32  LoadOptionsSize;
  VOID    *LoadOptions;
  // Location where image was loaded
  VOID            *ImageBase;
  UINT64          ImageSize;
  EFI_MEMORY_TYPE ImageCodeType;  /* Unused */
  EFI_MEMORY_TYPE ImageDataType;  /* Unused */
  EFI_IMAGE_UNLOAD Unload;        /* Currently Unsupported */
} EFI_LOADED_IMAGE_PROTOCOL;

typedef struct {
  EFI_IMAGE_ENTRY_POINT ImageEntry;
  UINT32  ImageSize;
  VOID* sectionEntry[IMAGE_SECTION_NUMBER];
  UINT32  sectionSize[IMAGE_SECTION_NUMBER];
  BOOLEAN  bStarted;
}EFI_LOAD_OPTIONS;

/*--------------------------------------------------------------------------
**                     Function Prototypes
**--------------------------------------------------------------------------
*/

EFI_STATUS LoadedImageEfiImageEntry(
  EFI_HANDLE image_handle,
  EFI_SYSTEM_TABLE *sys_table_ptr
  );

EFI_STATUS efi_loaded_image_open_protocol(
  EFI_BOOT_SERVICES *const boot_srv_ptr,
  EFI_LOADED_IMAGE_PROTOCOL **protocol
  );

#endif /* EFI_LOADED_IMAGE_PROTOCOL_H */
