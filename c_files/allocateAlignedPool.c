/*........................... FILE PROLOGUE ...........................*/

/*
 ***********************************************************************
 *
 * FILE NAME:
 *   allocateAlignedPool.c
 *
 * PURPOSE:
 *   This file includes implementation of AllocateAlignedPool
 *   functionality for allocating memory from heap for various
 *   pool types with desired alignment of address.
 *
 ***********************************************************************
 */

/*........................... FILE INCLUSION ..........................*/
#include "allocateAlignedPool.h"
/*......................... MACRO DEFINITIONS .........................*/

/*......................... TYPE DECLARATIONS .........................*/

/*....................... FUNCTION DECLARATIONS .......................*/

/*........................ OBJECT DEFINITIONS .........................*/ 

/*........................ FUNCTION DEFINITIONS .......................*/

/*
********************************************************************
* NAME:
*   AllocateAlignedPool
*
* PURPOSE:
*  Allocates memory from heap for the requested size and requested pool type, 
*  and aligns the start address of the allocated buffer to the desired alignment.
*
* PARAMETERS:
*   INPUT:
*     pSysTbl      -  Address of executing image system table
*     poolType     -  Type of memory to allocate
*     alignment    -  ppBuffer alignment in bytes i.e. 8, 16, 32, etc.
*     size         -  Size of the buffer to allocate in bytes
*
*   OUTPUT:
*     ppBuffer -  Address of the allocated buffer, aligned based on the input alignment
*
*   RETURNS:
*     EFI_SUCCESS            - The buffer is allocated and aligned based on the inputs size and alignment
*     EFI_INVALID_PARAMETER  - If any of the following are TRUE:
*                              * Parameter pSysTbl is NULL
*                              * Parameter size is 0
*                              * Parameter alignment is 0
*                              * (size + alignment - 1) wraps around
*                              * when AllocatePool() returns EFI_INVALID_PARAMETER i.e., if PoolType(poolType) 
*                                is invalid or Buffer(ppBuffer) is NULL. 
*
*     EFI_OUT_OF_RESOURCES   - AllocatePool() could not allocate memory for the required poolType.
*
* NOTES:
*   None
*
********************************************************************
*/
EFI_STATUS AllocateAlignedPool
(
  IN   EFI_SYSTEM_TABLE *pSysTbl,
  IN   EFI_MEMORY_TYPE poolType,
  IN   UINTN alignment,
  IN   UINTN size,
  OUT  VOID **ppBuffer
)
{
  EFI_STATUS status = EFI_NOT_FOUND;
  UINTN remainder = 0;
 
  /* <PSW-UTILS_LLR-98> */
  /* NULL checks */
  if ((pSysTbl == NULL) || (alignment == 0) || (size == 0))
  {
    status = EFI_INVALID_PARAMETER;
  }
  /* To check for overflow, rearranging the below expression,
     size + (alignment - 1) > UINTN_MAX */
  else if ((size - 1) > (UINTN_MAX - alignment))
  {
    status = EFI_INVALID_PARAMETER;
  }
  /* </PSW-UTILS_LLR-98> */
  /* <PSW-UTILS_LLR-115> */
  /* Allocate buffer from heap by invoking AllocatePool() boot service. */
  else
  {
    /* <PSW-UTILS_LLR-97> <PSW-UTILS_LLR-99> */
    status = pSysTbl->BootServices->AllocatePool(
                                                  poolType,
                                                  size + (alignment - 1),
                                                  ppBuffer
                                                );
    /* </PSW-UTILS_LLR-115> */
    /* Align ppBuffer according to input alignment if buffer allocation is successful */
    if (status == EFI_SUCCESS)
    {
      remainder = ((uintptr_t)(*ppBuffer)) % alignment;

      if (remainder != 0)
      {
        *ppBuffer = &((CHAR *)*ppBuffer)[alignment - remainder];
      }
    }
    /* </PSW-UTILS_LLR-97> </PSW-UTILS_LLR-99> */
  }
  return status;
}



