/*-----------------------------------------------------------------------------------------
**
** Copyright:  Copyright 2016 Rockwell Collins. All rights reserved.
**
** File:  elf.c
**
** Description:  This is an ELF loader function to load an executable ELF file from
**               a specified memory location to the virtual address specified in the
**               ELF header.  The loader branches to the program entry point when
**               loading is complete.
**
**-------------------------------------------------------------------------------------------
*/

/*-------------------------------------------------------------------------------------------
**                                     Include Files
**-------------------------------------------------------------------------------------------
*/

#include "elf.h"

/*-------------------------------------------------------------------------------------------
**                                  Literal Declarations
**-------------------------------------------------------------------------------------------
*/

/*-------------------------------------------------------------------------------------------
**                                    Type Definitions
**-------------------------------------------------------------------------------------------
*/

/*-------------------------------------------------------------------------------------------
**                                  Object Declarations
**-------------------------------------------------------------------------------------------
*/

extern EFI_BOOT_SERVICES *gBS;

struct node *head     = NULL;
UINT32      *LoadAddr = NULL;


/*-------------------------------------------------------------------------------------------
**                                  Function Prototypes
**-------------------------------------------------------------------------------------------
*/

/*-------------------------------------------------------------------------------------------
**                                  Externs
**-------------------------------------------------------------------------------------------
*/



/*-------------------------------------------------------------------------------------------
** Name:    load_elf_module
**
** Syntax:  ELF_LOAD_ERROR_TYPE load_elf_module(ELF32_ADDR *elf_addr)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  Loads an executable ELF module from a predefined memory location.
**
** Algorithm:   Validate ELF file and determine type
**              Load appropriate program header contents
**              Copy [.text] and [.data] sections from ELF exe file
**              Initialize [.bss] section with zeroes
**              Jump to entry point of newly loaded ELF executable
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE load_elf_module(ELF32_ADDR *elf_addr, EFI_IMAGE_ENTRY_POINT *elf_entry)
{
  ELF_LOAD_ERROR_TYPE error  = ELF_NO_ERROR;
  EFI_STATUS          status = EFI_OUT_OF_RESOURCES;

  ELF_MODULE_TYPE *ElfMod      = NULL;
  Elf32_Rel       *Relocations = NULL;
  UINT32          *Address     = NULL;
  UINT32           SymVal      = 0;
  UINTN            count;
  UINTN            idx;
  UINTN            Relentries;

  /* Shared library symbol declarations */
  bool   SharedLib = true;
  UINTN  SymCount  = 0;
  UINT8 *SymName   = NULL;
  UINT32 SymIdx    = 0;


  /* Allocate Memory in boot service Data pool for ELFMOD structure */
  status = gBS->AllocatePool(EfiBootServicesData, sizeof(ELF_MODULE_TYPE), (VOID **)&ElfMod);

  if (status != EFI_SUCCESS)
  {
    error = ELF_MEMORY_ALLOCATION_FAIL;
  }
  else
  {
    /* Set ELFHeader to start of elf address to get ELF Header */
    ElfMod->ELFheader = (struct elf32_ehdr *)elf_addr;

    /* Get ELF File Start of Symbol Section */
    ElfMod->Sections = (struct elf32_shdr *)(elf_addr +
                                             (ElfMod->ELFheader->e_shoff / 4));
    ElfMod->PgmHeader = (VOID *)((UINT32)elf_addr + (ElfMod->ELFheader->e_phoff / 4));

    /* validate ELF file header */
    if ((ElfMod->ELFheader->e_ident[0] != ELF_IDENT[0])   /* 0x7f */
        || (ElfMod->ELFheader->e_ident[1] != ELF_IDENT[1]) /* 'E'  */
        || (ElfMod->ELFheader->e_ident[2] != ELF_IDENT[2]) /* 'L'  */
        || (ElfMod->ELFheader->e_ident[3] != ELF_IDENT[3]) /* 'F'  */
        || (ElfMod->ELFheader->e_ident[4] != ELFCLASS32)
        || (ElfMod->ELFheader->e_ident[5] != ELFDATA2LSB))
    {
      error = ELF_INCORRECT_FILE_IDENT;
    }
    else
    {
      /* Validate ELF is for ARM */
      if (ElfMod->ELFheader->e_machine != EM_ARM)
      {
        error = ELF_INCORRECT_MODULE_ARCH;
        return (error);
      }

      /* ELF File Type */
      switch (ElfMod->ELFheader->e_type)
      {
        case ET_DYN: /* Loads Generated Dynamic ELF File*/

          error = load_sections(ElfMod, elf_entry, &SharedLib, &SymCount);

          if (ELF_NO_ERROR == error)
          {
            /* save symbols and strings for shared library for further relocations */
            if (true == SharedLib)
            {
              /*  save symbol values into link list */
              error = shared_lib_symbollist(LoadAddr, ElfMod, SymCount);

              if (ELF_NO_ERROR != error)
              {
                error = ELF_SHARED_LIB_LOAD_FAIL;
                break;
              }
            }

            /* Apply Relocations on Required Symbols */
            for (count = 0; count < ElfMod->ELFheader->e_shnum; count++)
            {
              /* Check if Section is of Relocation Type */
              if (ElfMod->Sections[count].sh_type == SHT_REL)
              {
                /* Get Relocation address , required to relocate */
                Relocations = (Elf32_Rel *)(elf_addr +
                                            (ElfMod->Sections[count].sh_offset / 4));

                /*Get No of Relocation Entry from section */
                Relentries = ElfMod->Sections[count].sh_size / ElfMod->Sections[count].sh_entsize;

                /* loop through all relocation entries and apply address fix ups */
                for (idx = 0; idx < Relentries; idx++)
                {
                  /* if relocations are for PIE & dynamic apply relocations */
                  if ((ELF32_R_TYPE(Relocations[idx].r_info) == R_ARM_GLOB_DAT)
                      || (ELF32_R_TYPE(Relocations[idx].r_info) == R_ARM_JUMP_SLOT)
                      || (ELF32_R_TYPE(Relocations[idx].r_info) == R_ARM_ABS32))
                  {
                    if (true == SharedLib)
                    {
                      /* Read Symbol index and Get Symbol Value */
                      Address = LoadAddr;
                      SymVal  = (UINT32)LoadAddr;

                      /* Get Symbol value */
                      SymIdx = ELF32_R_SYM(Relocations[idx].r_info);

                      /* symbol value should not be zero , Add Symbol value to load address and
                       * Apply Relocation fix ups */
                      if (SymIdx)
                      {
                        SymVal  += ElfMod->SymbolTbl[SymIdx].st_value;
                        Address += (Relocations[idx].r_offset) / 4;
                        gBS->CopyMem(Address, &SymVal, sizeof(SymVal));
                      }
                      else
                      {
                        error = ELF_RELOCATION_FAIL;
                        break;
                      }
                    }
                    else
                    {
                      /* Relocations for shared library */
                      /*Get Symbol Index */
                      SymIdx   = ELF32_R_SYM(Relocations[idx].r_info);
                      SymName  = (UINT8 *)(ElfMod->StringTbl);
                      SymName += ElfMod->SymbolTbl[SymIdx].st_name;

                      SymVal = get_symbol(SymName);

                      if (0 == SymVal)
                      {
                        error = ELF_SHARED_SYM_GET_FAIL;
                        break;
                      }
                      else
                      {
                        /* Update relocatable address with Shared lib address */
                        Address  = LoadAddr;
                        Address += (Relocations[idx].r_offset) / 4;
                        gBS->CopyMem(Address, &SymVal, sizeof(SymVal));
                      }
                    }
                  }
                  else if (R_ARM_RELATIVE == ELF32_R_TYPE(Relocations[idx].r_info))
                  {
                    /* Read Symbol index and Get Symbol Value */
                    Address = LoadAddr;
                    SymVal  = (UINT32)LoadAddr;

                    Address += (Relocations[idx].r_offset) / 4;
                    SymVal  += *Address;
                    gBS->CopyMem(Address, &SymVal, sizeof(SymVal));
                  }
                } /* for loop for REL Entries */

              } /* if relocation section Found */

              if (ELF_NO_ERROR != error)
              {
                break;
              }
            } /* for loop through Sections */

          }

          break; /* Shared object file type. */

        case ET_EXEC: /* Executable ELF */
#if 0
          *elf_entry = (EFI_IMAGE_ENTRY_POINT)ElfMod->ELFheader->e_entry;

          for (count = 0; count < ElfMod->ELFheader->e_phnum; count++)
          {
            ELF32_PHDR_TYPE *phdr = &ElfMod->PgmHeader[count];

            if (phdr->p_type == PT_LOAD)
            {
              source_addr = *elf_addr + (phdr->p_offset / 4);
              gBS->CopyMem((void *)phdr->p_vaddr, (void *)source_addr, phdr->p_filesz);

              /* TODO: Resolve issue where setting bss section to zero freezes execution */
              /* initialize extra bytes in segment memory size [.bss section] to zero */

              //memset((void *)(phdr->p_vaddr + phdr->p_filesz), 0, (phdr->p_memsz - phdr->p_filesz));
            }
          }

#endif
          break;

        case ET_REL:
        default:
          error = ELF_UNKNOWN_FILE_TYPE;
          break;
      } /* end Switch Case */

    } /* end if not valid elf header */

  } /* end if ELF MOD Structure memory allocation failed */

  return (error);
}

/*-------------------------------------------------------------------------------------------
** Name:    load_sections
**
** Syntax:  ELF_LOAD_ERROR_TYPE load_sections (ELF_MODULE_TYPE *ElfMod, EFI_IMAGE_ENTRY_POINT *elf_entry,
**                                              bool *SharedLib , UINTN *SymCount)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  Loads an shared PIE/PIC ELF module from a predefined memory location.
**
** Algorithm:   Copy [.text] and [.data] sections from ELF exe file
**              Initialize [.bss] section with zeroes
**              Jump to entry point of newly loaded ELF executable
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE load_sections(ELF_MODULE_TYPE *ElfMod, EFI_IMAGE_ENTRY_POINT *elf_entry,
                                  bool *SharedLib, UINTN *SymCount)
{
  ELF_LOAD_ERROR_TYPE error  = ELF_NO_ERROR;
  EFI_STATUS          status = EFI_OUT_OF_RESOURCES;

  void   *FileAddr    = (UINT8 *)ElfMod->ELFheader;
  UINT32 *Address     = NULL;
  UINT32 *EntAddr     = NULL;
  UINT32  SecOffset   = 0;
  UINT8  *SecName     = NULL;
  UINTN   SecSize     = 0;
  UINTN   EntrySize   = 0;
  UINTN   count       = 0;
  UINTN   idx         = 0;
  UINTN   PrevAddr    = 0;
  UINTN   CurrentAddr = 0;
  bool    FirstSec    = true;

  /* Get Size of All Valid Sections to allocate dynamic Memory */
  for (count = 0; count < ElfMod->ELFheader->e_shnum; count++)
  {
    idx = ElfMod->Sections[count].sh_type;

    if ((ElfMod->Sections[count].sh_type == SHT_PROGBITS)
        || (ElfMod->Sections[count].sh_type == SHT_REL)
        || (ElfMod->Sections[count].sh_type == SHT_DYNAMIC)
        || (ElfMod->Sections[count].sh_type  == SHT_DYNSYM)
        || (ElfMod->Sections[count].sh_type  == SHT_HASH)
        || (ElfMod->Sections[count].sh_type == SHT_STRTAB)
        || ((ElfMod->Sections[count].sh_type == SHT_NOBITS)
            && (ElfMod->Sections[count].sh_flags & SHF_ALLOC)))
    {
      if (ElfMod->Sections[count].sh_size)
      {
        /* Calculate size of all segment based on addresses */

        /*NOTE : Calculated based on Address instead section size
         *       to consider padding between sections for PIE/PIC */
        CurrentAddr = ElfMod->Sections[count].sh_addr;

        if (0 != CurrentAddr)
        {
          SecSize += CurrentAddr - PrevAddr;
          PrevAddr = CurrentAddr;
        }
        else if ((0 == CurrentAddr) && (0 != PrevAddr))
        {
          SecSize += ElfMod->Sections[count].sh_size;
          PrevAddr = CurrentAddr;
        }

        /* Set Address offset at start */
        if (true == FirstSec)
        {
          SecOffset = ElfMod->Sections[count].sh_addr;
          FirstSec  = false;
        }
      }
      else
      {
        error = ELF_KNOWN_SEC_SIZE_ERROR;
      }
    }
  }

  /*Allocate Dynamic memory for ELF Sections*/
  if (ELF_NO_ERROR == error)
  {
    status = gBS->AllocatePool(EfiBootServicesCode, SecSize, (VOID **)&LoadAddr);

    if (status != EFI_SUCCESS)
    {
      error = ELF_MEMORY_ALLOCATION_FAIL;
    }
    else
    {
      Address     = LoadAddr;
      idx         = ElfMod->ELFheader->e_shstrndx;
      PrevAddr    = 0;
      CurrentAddr = 0;

      /*Set offset bytes to Zero */
      gBS->SetMem((VOID *)Address, SecOffset, 0);
      Address += SecOffset / 4;

      /* Loop to get sections one by one and load into RAM Allocated Memory */
      for (count = 0; count < ElfMod->ELFheader->e_shnum; count++)
      {
        SecName     = FileAddr;
        SecName    += ElfMod->Sections[idx].sh_offset;
        SecName    += ElfMod->Sections[count].sh_name;
        CurrentAddr = ElfMod->Sections[count + 1].sh_addr;

        /* Calculate Section Size based on Section Addresses */
        if (0 != CurrentAddr)
        {
          SecSize  = CurrentAddr - PrevAddr;
          PrevAddr = CurrentAddr;
        }
        else if ((0 == CurrentAddr) && (0 != PrevAddr))
        {
          SecSize  = ElfMod->Sections[count].sh_size;
          PrevAddr = CurrentAddr;
        }
        else
        {
          SecSize = 0;
        }

        /* Write Section if Section Size is not Zero */
        if (SecSize)
        {
          /* Check if its required sections to copy */
          if ((ElfMod->Sections[count].sh_type == SHT_PROGBITS)
              || (ElfMod->Sections[count].sh_type == SHT_REL)
              || (ElfMod->Sections[count].sh_type == SHT_DYNAMIC)
              || (ElfMod->Sections[count].sh_type  == SHT_DYNSYM)
              || (ElfMod->Sections[count].sh_type  == SHT_HASH)
              || (ElfMod->Sections[count].sh_type == SHT_STRTAB))
          {
            gBS->CopyMem((VOID *)Address, (VOID *)((UINT8 *)FileAddr + ElfMod->Sections[count].sh_offset),
                         ElfMod->Sections[count].sh_size);

            /* if Section is of PROGBITS type Look for Text Section */
            if (ElfMod->Sections[count].sh_type == SHT_PROGBITS)
            {
              /* set Entry Point to start of Code */
              if (0 == gBS->CompareMem(SecName, ".text", 5))
              {
                EntrySize = ElfMod->ELFheader->e_entry - ElfMod->Sections[count].sh_addr;
                EntAddr   = Address;

                if (0 < EntrySize)
                {
                  EntAddr += EntrySize / 4;
                }


                *elf_entry =  (EFI_IMAGE_ENTRY_POINT)EntAddr;
              }

              /* Check if the Shared object is shared library or Shared Executable */
              if (0 == gBS->CompareMem(SecName, ".interp", 7))
              {
                *SharedLib = false;
              }
            }

            /* Add Section Size to move to Next Section */
            /* sh_size is no of bytes , divided by 4 to increment address pointer */
            Address += SecSize / 4;
          }
          /* Allocate Uninitialized data section for PIE */
          else if ((ElfMod->Sections[count].sh_type == SHT_NOBITS)
                   && (ElfMod->Sections[count].sh_flags & SHF_ALLOC))
          {
            gBS->SetMem(Address, SecSize, 0);
            Address += SecSize / 4;
          }

          /* Get Address Pointer for Dynamic Symbol */
          if (ElfMod->Sections[count].sh_type  == SHT_DYNSYM)
          {
            ElfMod->SymbolTbl = (struct elf32_sym *)((UINT32 *)FileAddr +
                                                     (ElfMod->Sections[count].sh_offset / 4));

            /* List no of symbols used for shared lib symbol load */
            *SymCount = ElfMod->Sections[count].sh_size / ElfMod->Sections[count].sh_entsize;
          }

          if ((ElfMod->Sections[count].sh_type  == SHT_STRTAB)
              && (0 == gBS->CompareMem(SecName, ".dynstr", 7)))
          {
            ElfMod->StringTbl = (struct elf32_shdr *)((UINT32 *)FileAddr +
                                                      (ElfMod->Sections[count].sh_offset / 4));
          }
        }
      }
    }
  }

  return (error);
}

/*-------------------------------------------------------------------------------------------
** Name:    shared_lib_symbollist
**
** Syntax:  ELF_LOAD_ERROR_TYPE shared_lib_symbollist (VOID *ELFLoadAddr, ELF_MODULE_TYPE
**                                                      *ElfMod,  UINTN SymCount)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  creates new node for shared library to story symbols and load address
**
** Algorithm:
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE shared_lib_symbollist(VOID *ELFLoadAddr, ELF_MODULE_TYPE *ElfMod,
                                          UINTN SymCount)
{
  ELF_LOAD_ERROR_TYPE ret    = ELF_NO_ERROR;
  EFI_STATUS          status = EFI_OUT_OF_RESOURCES;

  struct node *NewLibNode = NULL;
  struct node *current    = NULL;

  status = gBS->AllocatePool(EfiBootServicesCode, sizeof(struct node), (VOID **)&NewLibNode);

  if (status != EFI_SUCCESS)
  {
    ret = ELF_MEMORY_ALLOCATION_FAIL;
  }
  else
  {
    if (NULL == head)
    {
      head    = NewLibNode;
      current = NewLibNode;
    }
    else
    {
      current->next = NewLibNode;
      current       = NewLibNode;
    }

    current->next          = NULL;
    current->SymCount      = 0;
    current->SymbolTblAddr = NULL;

    /* Provide shared lib load address and Symbols*/
    current->SharedLoadAddr = (UINT32)ELFLoadAddr;
    ret                     = write_symbols(current, ElfMod, SymCount);
  } /* Shared NewLibNode Allocation */

  return (ret);
}

/*-------------------------------------------------------------------------------------------
** Name:    write_symbols
**
** Syntax:  ELF_LOAD_ERROR_TYPE write_symbols (VOID *NewLibNode, ELF_MODULE_TYPE *ElfMod,
**                                                UINTN SymCount)
**
** Returns: ELF_LOAD_ERROR_TYPE
**
** Description:  Write Shared library symbol name and addresses into node.
**
** Algorithm:
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
ELF_LOAD_ERROR_TYPE write_symbols(VOID *NewLibNode, ELF_MODULE_TYPE *ElfMod, UINTN SymCount)
{
  ELF_LOAD_ERROR_TYPE ret    = ELF_NO_ERROR;
  EFI_STATUS          status = EFI_OUT_OF_RESOURCES;

  struct node *Node         = NewLibNode;
  bool         DuplicateSym = false;
  UINT32       WrittenSym   = 0;
  UINTN        Countloop    = 0;
  UINTN        Count        = 0;
  UINTN        ValidSym     = 0;

  UINT8  SymTyp   = 0;
  UINT8 *SymStrng = NULL;
  UINT32 p        = 0;

  /* Loop through all Symbols  and count no of symbols required for */
  for (Countloop = 0; Countloop < SymCount; Countloop++)
  {
    SymTyp = (ElfMod->SymbolTbl[Countloop].st_info & 0xf);

    if (SymTyp == 1 || SymTyp == 2) /* STT_OBJECT or STT_FUNC */
    {
      ValidSym++;
    }
  }

  /* Allocate structure memory for valid symbols */
  status = gBS->AllocatePool(EfiBootServicesCode, sizeof(SHARED_SYM) * ValidSym,
                             (VOID **)&Node->SymbolTblAddr);

  if (status != EFI_SUCCESS)
  {
    ret = ELF_MEMORY_ALLOCATION_FAIL;
  }
  else
  {
    /*Loop through Symbols */
    for (Countloop = 0; Countloop < SymCount; Countloop++)
    {
      SymTyp = (ElfMod->SymbolTbl[Countloop].st_info & 0xf);

      if (SymTyp == 1 || SymTyp == 2) /* STT_OBJECT or STT_FUNC */
      {
        SymStrng  = (UINT8 *)ElfMod->StringTbl;
        SymStrng += ElfMod->SymbolTbl[Countloop].st_name;

        p = sizeof((const char *)SymStrng);

        /* check for prior definition of symbol */
        for (Count = 0; Count < WrittenSym; Count++)
        {
          if (0 == gBS->CompareMem(Node->SymbolTblAddr[Count].names, SymStrng, p))
          {
            DuplicateSym = true;
          }
        }

        if (!DuplicateSym)
        {
          /* Allocate memory for Symbol  Name */
          status = gBS->AllocatePool(EfiBootServicesCode, p, (VOID **)
                                     &Node->SymbolTblAddr[WrittenSym].names);

          if (status == EFI_SUCCESS)
          {
            gBS->CopyMem(Node->SymbolTblAddr[WrittenSym].names, SymStrng, p);
           
            /*Set Symbol Address */
            Node->SymbolTblAddr[WrittenSym].address  = Node->SharedLoadAddr;
            Node->SymbolTblAddr[WrittenSym].address += ElfMod->SymbolTbl[Countloop].st_value;
            WrittenSym++;
          }
          else
          {
            ret = ELF_MEMORY_ALLOCATION_FAIL;
            break;
          }
        }
        else
        {
          DuplicateSym = false;
        }
      }
    }

    if (ELF_NO_ERROR == ret)
    {
      Node->SymCount = (UINT32)WrittenSym;
    }
  }

  return (ret);
}

/*-------------------------------------------------------------------------------------------
** Name:    get_symbol
**
** Syntax: UINT32 get_symbol (VOID *SymName)
**
** Returns: UINT32 : Address of Shared library symbol
**
** Description:  Get Shared library symbol address based on symbol name
**
** Algorithm:
**
** Notes:
**
**-------------------------------------------------------------------------------------------
*/
UINT32 get_symbol(VOID *SymName)
{
  struct node *current = head;
  UINT32       Count   = 0;
  UINT32       SymVal  = 0;

  while (NULL != current)
  {
    /*Compare Symbol with Library Symbols */
    for (Count = 0; Count < current->SymCount; Count++)
    {
      if (0 == gBS->CompareMem(current->SymbolTblAddr[Count].names,
                               SymName, sizeof((const char *)SymName)))
      {
        SymVal = current->SymbolTblAddr[Count].address;
        break;
      }
    }

    if (0 != SymVal)
    {
      break;
    }

    current = current->next;
  }

  return (SymVal);
}

